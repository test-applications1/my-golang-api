package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"quickstart/models"
	"quickstart/util"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {

	// initialize router
	router := mux.NewRouter().StrictSlash(true)
	// endpoints
	router.HandleFunc("/dd-api/sys/healthCheck", healthCheck).Methods("GET")
	router.HandleFunc("/dd-api/register", registerAccount).Methods("POST")
	router.HandleFunc("/dd-api/accounts/{id}", getAccountById).Methods("GET")
	router.HandleFunc("/dd-api/accounts", getAccountByAccNo).Queries("accNum", "{accNum}").Methods("GET")
	router.HandleFunc("/dd-api/accounts", getAccounts).Methods("GET")

	// set the server
	fmt.Println("Server running on 8080")
	http.ListenAndServe(":8080", router)

}

func getAccounts(responseWriter http.ResponseWriter, request *http.Request) {
	// set response header
	responseWriter.Header().Set("Content-Type", "application.json")
	ctx := util.GetContext()

	// create a array of accounts to store the res
	var accounts []models.Account

	// get the collection
	collection := util.DbConnection()

	// get all data by passing empty filter
	cursor, err := collection.Find(ctx, bson.M{})

	if err != nil {
		util.GetError(err, responseWriter)
		return
	}

	// close the cursor after completing
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var account models.Account
		// decode to account
		err := cursor.Decode(&account)

		if err != nil {
			log.Fatal(err.Error())
		}
		// add account to array
		accounts = append(accounts, account)
	}

	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// encode
	json.NewEncoder(responseWriter).Encode(accounts)
}

func getAccountById(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")
	ctx := util.GetContext()

	// account to be return
	var account models.Account

	// get the id parameter from Mux
	var params = mux.Vars(request)
	// convert it to mongodb ObjectId
	id, _ := primitive.ObjectIDFromHex(params["id"])

	collection := util.DbConnection()
	filter := bson.M{"_id": id}

	err := collection.FindOne(ctx, filter).Decode(&account)

	if err != nil {
		util.GetError(err, responseWriter)
		return
	}

	json.NewEncoder(responseWriter).Encode(account)
}

func getAccountByAccNo(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")
	ctx := util.GetContext()

	// account to be return
	var account models.Account

	// get the id parameter from Mux
	//var params = mux.Vars(request)
	// convert it to mongodb ObjectId
	//accNo,_ := primitive.ObjectIDFromHex(params["accNum"])
	accNo := request.FormValue("accNum")
	collection := util.DbConnection()
	filter := bson.M{"accno": accNo}

	err := collection.FindOne(ctx, filter).Decode(&account)

	if err != nil {
		util.GetError(err, responseWriter)
		return
	}

	json.NewEncoder(responseWriter).Encode(account)
}

func registerAccount(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")
	ctx := util.GetContext()

	var account models.Account

	// decode the req body params
	_ = json.NewDecoder(request.Body).Decode(&account)
	collection := util.DbConnection()

	account.ID = primitive.NewObjectID()

	// insert the account
	resp, err := collection.InsertOne(ctx, account)

	if err != nil {
		util.GetError(err, responseWriter)
		return
	}
	json.NewEncoder(responseWriter).Encode(resp)

}

func healthCheck(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

	response := map[string]string{
		"healthy": "true",
	}
	message, _ := json.Marshal(response)

	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Write(message)
	return
}
