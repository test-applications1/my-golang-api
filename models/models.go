package models

import "go.mongodb.org/mongo-driver/bson/primitive"

//struct
type Account struct {
	ID primitive.ObjectID `json:"_id,omitempty" bson:"_id, omitempty"`
	FirstName string `json:"firstname,omitempty" bson:"firstname,omitempty"`
	LastName string `json:"lastname, omitempty" bson:"lastname, omitempty"`
	AccNo string `json:"accno,omitempty" bson:"accno,omitempty"`
	AccType string `json:"acctype,omitempty" bson:"acctype,omitempty"`
	Active bool `json:"active,omitempty" bson:"active,omitempty"`
	BankName string `json:"bankname,omitempty" bson:"bankname,omitempty"`
	RoutingNumber int `json:"rountingnumber,omitempty" bson:"rountingnumber,omitempty"`
}
