## My-GoLang-API


#### [API Documentation](https://documenter.getpostman.com/view/7740755/SzmZcLHq?version=latest)

- To run this application, we need mongodb running.
- Running mongo in a docker container.
```shell script
cd mongo-db
mongo-db % docker build -t mongodb .
docker run --rm -p 27017:27017 mongodb
```
- Now, from new terminal, build and run the API
```shell script
direct-deposit-api % go build main.go
direct-deposit-api % go run ./
Server running on 8080

```


