package util

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func DbConnection() *mongo.Collection {

	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)

	// set the mongo client
	mongoClientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.NewClient(mongoClientOptions)
	ctx, cancel:=context.WithTimeout(context.Background(), 50*time.Second)
	 err = client.Connect(ctx)

	if err != nil {
		log.Fatal(err)
		defer cancel()
	}
	fmt.Println("Connection successful")

	collection := client.Database("direct_deposit").Collection("accounts")
	return collection
}

// model to handle error
type ErrorResponse struct {
	StatusCode int `json:"status"`
	ErrorMessage string `json:"message"`
}

// helper function to prepare error model
func GetError(err error, writer http.ResponseWriter)  {

	// log the error
	log.Fatal(err.Error())
	// create the response
	var response = ErrorResponse{
		ErrorMessage: err.Error(),
		StatusCode: http.StatusInternalServerError,
	}

	// convert to json
	message, _ := json.Marshal(response)
	writer.WriteHeader(response.StatusCode)
	writer.Write(message)
}

func GetContext() context.Context {
	ctx, _ := context.WithTimeout(context.Background(), 50*time.Second)
	return ctx
}